@extends('layouts.app')

@section('content')
<div class="row">
@foreach($blogs as $blog)


<div class="col-md-12">
<br>
<br>
<div class="card">

<div class="card-header">
       <a href="{{route('blog_path',['blog'=>$blog->id])}}">{{$blog->title}}</a> 
      
</div>
<div class="card-body">
<div class="row">
<div class="col-md-6">
<a href="{{route('blog_path',['blog'=>$blog->id])}}">
<img src="{{asset($blog->image)}}" alt="" class="card-img-top" style="height:200px; width: 450px">
</a>
</div>
<div class="col-md-6">
<p class="lead">
Posting Time
  {{$blog->created_at->diffForHumans()}}
</p>


Description:
{{$blog->content}}
<br>
<br>
<br>
<br><br>
<a href="{{route('blog_path',['blog'=>$blog->id])}}" class="btn btn-outline-primary">View Post</a>
</div>
</div>

</div>
</div>




</div>
@endforeach


</div>



@endsection