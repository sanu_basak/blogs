@extends('layouts.app')
@section('content')

<div class="row container"  > 
<div class="col-md-10" >
<br>
<img src="{{asset($blog->image)}}" alt="" class="card-img-top" style="height:200px; width: 450px" >
<br><br>
<h4>{{$blog->title}}</h4>
<hr>
<p class="lead">
{{$blog->content}}
</p>
<div class="row inline">
<a href="{{route('edit_blog_path',$blog->id)}}" class="btn btn-outline-info">Edit</a>
<a href="{{route('blogs_path')}}" class="btn btn-outline-secondary">Back</a>
<form  action="{{route('delete_blog_path')}}" method="POST">
@csrf 
@method('DELETE')
<input type="hidden" value="{{$blog->id}}" name="blog_id" id="blog_id">
<button class="btn btn-outline-danger " type="submit">Detele</button>
</form>
</div>
<hr>
<h3>Add Comment </h3>

<form method="POST" action="{ {route('add_comment_path')} }">
                        @csrf
                      
                        <div class="form-group">
                            <input type="text" name="comment_body" class="form-control" />
                            <input type="hidden" name="blog_id" value="{{$blog->id}}" />
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="submit" class="btn btn-warning" value="Add Comment" />
                        </div>
                        <div class="form-group">
                         
                        @foreach ($blog->comment as $comment)
                        <h5>UserName : {{$comment->user->name}}</h5>
                            <h5>{{$comment->description}}</h5>

                            
                             
                               <div>   
                                   <span onclick= "myFunction({{$comment->id}});" ><b>Reply</b></span>

                                   <div id="reply-from{{$comment->id}}" style="display:none">
                                       <form  data-route="{{route('add_reply_path')}}" method="POST" id="replyForms-{{$comment->id}}">
                                       @csrf
                                       <input type ="text" name ="description" id="description-{{$comment->id}}" class="form-control"/>
                                       <input type="hidden" name="user_id" id="user_id-{{$comment->id}}" value="{{Auth::user()->id}}">
                                       <input type="hidden" value="{{$comment->id}}" id="comment-{{$comment->id}}" name="comment_id" id="comment_id">
                                       <span id="myreply{{$comment->id}}" class="btn btn-primary" onclick="ajaxFormSubmit(event,{{$comment->id}});">Add Reply</span>
                                       </form>
                                   </div>
                             </div>
                            <!-- <form  action="{{route('add_reply_path')}}" method="POST">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <textarea name="content"  rows="10" class="form-control" >{{$blog->content}}</textarea> 
                            <input type="hidden" value="{{$comment->id}}" name="comment_id" id="comment_id">
                            
                           ///////                           /// onsubmit="chacha(event);""
                            </form> -->
                         @endforeach     
                         
                        </div>
                    </form>

                   
</div>
</div>

@endsection
@section('script')

<script type="text/javascript">
function myFunction(id) {
  document.getElementById("reply-from"+id).style.display = "block";
}

function ajaxFormSubmit(e,id){
    e.preventDefault();
//     var form = document.getElementById('replyForms-'+id)

//    var formData = new FormData(form);

               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': "{{ csrf_token() }}"
                  }
              });
               $.ajax({
                  url: "{{ route('add_reply_path') }}",
                  method: 'POST',
                  data: {
                     data: document.getElementById('user_id-'+id),
                     comment_id  : document.getElementById('comment-'+id)
                   },
                  success: function(result){
                     console.log(result);
                  }
               });






}
// $(document).ready(function(){
//     $('#replyForms').submit(function() {
//         alert("hello");

      
    // get all the inputs into an array.
    //var $inputs = $('#replyForms :input');
    

    // not sure if you wanted this, but I thought I'd add it.
    // get an associative array of just the values.
    // var values = {};
    // $inputs.each(function() {
    //     values[this.name] = $(this).val();
    // });

// });
// })
// function myreply(id)
// {
// alert(id);
// }
</script>
@endsection